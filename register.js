import http from 'k6/http';
import { fixtures } from './fixtures.js';

export default function () {
    const url = `${fixtures.baseUrl}/api/create-account`
    const random = Math.random().toString().replace('.', '');
    const body = {
      server_key: fixtures.serverKey,
      username: `testuser${random}`,
      email: `${random}test@mail.com`,
      password: '123456789',
      confirm_password: '123456789',
      ref: 'admin',
      device_type: 'windows'
    };
    const response = http.post(url, body);
    const info = {
        request: body,
        response: response.body
    }
    console.log(JSON.stringify(info, null, 2));
}
