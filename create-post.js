import http from 'k6/http';
import { fixtures } from './fixtures.js';

export default function () {
    const url = `${fixtures.baseUrl}/app_api.php?application=phone&type=new_post`;
    const random = Math.random().toString().replace('.', '');
    const body = {
        user_id: 4931,
        s: '554909a412351a5b67980f50c8cecbc7b1e5d9ce921862ee6a02c8af8632cc2f92244304943257472c60e40b399dc55d8b755ec6b5d09f8a',
        postText: random + random + random + random
    };
    const response = http.post(url, body);
    const info = {
        request: body,
        response: response.body
    }
    console.log(JSON.stringify(info, null, 2));
}
