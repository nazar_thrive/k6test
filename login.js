import http from 'k6/http';
import { fixtures } from './fixtures.js';

export default function () {
    const url = `${fixtures.baseUrl}/api/auth`;
    const body = {
        server_key: fixtures.serverKey,
        username: 'testuser',
        password: 'testpassword',
        device_type: 'windows'
    };
    const response = http.post(url, body);
    const info = {
        request: body,
        response: response.body
    }
    console.log(JSON.stringify(info, null, 2));
}
